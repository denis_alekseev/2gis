<?php

namespace app\controllers;

use app\models\Building;
use app\models\Organization;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;

class BuildingsController extends ActiveController
{
    public $modelClass = 'app\models\Building';

    /**
     * переопределяем стнадартный метод для просмотра одного здания
     * метод для просмотра списка зданий используем стандартный
     * все остальные методы заблокированы в роутере
     */
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['view']);
        return $actions;
    }

    /**
     * @url /buildings/{id}
     * @param $id integer - id здания которое требуется просмотреть
     * возвращает в формате JSON координаты, адрес и список организаций находящихся в здани
     */
    public function actionView($id)
    {
        $res = Building::findOne(['id'=>$id]);
        $org = array_map( function($data){return $data;},
            Building::findOne(['id'=>$id])->getOrganizations()->select(['id','name'])->cache(3600)->all());

        $res->orgs = $org;
        return $res;
    }
}
