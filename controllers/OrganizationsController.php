<?php

namespace app\controllers;

use app\models\Building;
use app\models\Organization;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use Yii;

class OrganizationsController extends ActiveController
{
    public $modelClass = 'app\models\Organization';

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        unset($actions['view']);
        return $actions;
    }

    public function actionIndex()
    {
        $params = Yii::$app->getRequest()->getBodyParams();
        if (empty($requestParams)) {
            $params = Yii::$app->getRequest()->getQueryParams();
        }
        // На самом деле дальше идёт кусок не красивого кода и всё надо делать через абстрактную фабрику
        // Но я сделаю простым свичем для экономии времени
        $res = new Organization();
        if (isset($params['radius']) && isset($params['center'])) {
            $points = $res->getAllInRadius();

        } elseif (isset($params['coords'])) {
            $points = $res->getAllInRectalngle();
        } elseif(isset($params['category'])) {
            $points = $res->getByCategory();
        }
        else {
            $points = $res->getAll();
        }
        return $points;
    }



    public function actionView($id)
    {
        $res = Organization::findOne(['id'=>$id])->cache(3600);
        return $res;
    }
}
