
DESCRIPTION

This generator generates an ActiveRecord class for the specified database
table.


USAGE

yii gii/model [...options...]


OPTIONS

--appconfig: string
  custom application configuration file path.
  If not set, default application configuration is used.

--baseClass: string (defaults to 'yii\db\ActiveRecord')
  This is the base class of the new ActiveRecord class. It should be a fully
  qualified namespaced class name.

--color: boolean, 0 or 1
  whether to enable ANSI color in the output.
  If not set, ANSI color will only be enabled for terminals that support it.

--db: string (defaults to 'db')
  This is the ID of the DB application component.

--enableI18N: boolean, 0 or 1 (defaults to 0)
  This indicates whether the generator should generate strings using Yii::t()
  method. Set this to true if you are planning to make your application
  translatable.

--generateLabelsFromComments: boolean, 0 or 1 (defaults to 0)
  This indicates whether the generator should generate attribute labels by
  using the comments of the corresponding DB columns.

--generateQuery: boolean, 0 or 1 (defaults to 0)
  This indicates whether to generate ActiveQuery for the ActiveRecord class.

--generateRelations: string (defaults to 'all')
  This indicates whether the generator should generate relations based on
  foreign key constraints it detects in the database. Note that if your
  database contains too many tables, you may want to uncheck this option to
  accelerate the code generation process.

--generateRelationsFromCurrentSchema: boolean, 0 or 1 (defaults to 1)
  This indicates whether the generator should generate relations from current
  schema or from all available schemas.

--help, -h: boolean, 0 or 1
  whether to display help information about current command.

--interactive: boolean, 0 or 1 (defaults to 1)
  whether to run the command interactively.

--messageCategory: string (defaults to 'app')
  This is the category used by Yii::t() in case you enable I18N.

--modelClass: string
  This is the name of the ActiveRecord class to be generated. The class name
  should not contain the namespace part as it is specified in "Namespace".
  You do not need to specify the class name if "Table Name" ends with
  asterisk, in which case multiple ActiveRecord classes will be generated.

--ns: string (defaults to 'app\models')
  This is the namespace of the ActiveRecord class to be generated, e.g.,
  app\models

--overwrite: boolean, 0 or 1 (defaults to 0)
  whether to overwrite all existing code files when in non-interactive mode.
  Defaults to false, meaning none of the existing code files will be overwritten.
  This option is used only when `--interactive=0`.

--queryBaseClass: string (defaults to 'yii\db\ActiveQuery')
  This is the base class of the new ActiveQuery class. It should be a fully
  qualified namespaced class name.

--queryClass: string
  This is the name of the ActiveQuery class to be generated. The class name
  should not contain the namespace part as it is specified in "ActiveQuery
  Namespace". You do not need to specify the class name if "Table Name" ends
  with asterisk, in which case multiple ActiveQuery classes will be
  generated.

--queryNs: string (defaults to 'app\models')
  This is the namespace of the ActiveQuery class to be generated, e.g.,
  app\models

--singularize: boolean, 0 or 1 (defaults to 0)
  This indicates whether the generated class names should be singularized.
  For example, table names like some_tables will have class names SomeTable.

--standardizeCapitals: boolean, 0 or 1 (defaults to 0)
  This indicates whether the generated class names should have standardized
  capitals. For example, table names like SOME_TABLE or Other_Table will have
  class names SomeTable and OtherTable, respectively. If not checked, the
  same tables will have class names SOMETABLE and OtherTable instead.

--tableName (required): string
  This is the name of the DB table that the new ActiveRecord class is
  associated with, e.g. post. The table name may consist of the DB schema
  part if needed, e.g. public.post. The table name may end with asterisk to
  match multiple table names, e.g. tbl_* will match tables who name starts
  with tbl_. In this case, multiple ActiveRecord classes will be generated,
  one for each matching table name; and the class names will be generated
  from the matching characters. For example, table tbl_post will generate
  Post class.

--template: string (defaults to 'default')

--useSchemaName: boolean, 0 or 1 (defaults to 1)
  This indicates whether to include the schema name in the ActiveRecord class
  when it's auto generated. Only non default schema would be used.

--useTablePrefix: boolean, 0 or 1 (defaults to 0)
  This indicates whether the table name returned by the generated
  ActiveRecord class should consider the tablePrefix setting of the DB
  connection. For example, if the table name is tbl_post and
  tablePrefix=tbl_, the ActiveRecord class will return the table name as
  {{%post}}.

