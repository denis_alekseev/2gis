<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\db\Exception;
use yii\db\Query;

/**
 * This is the model class for table "organization".
 *
 * @property int $id
 * @property string|null $name
 */
class Organization extends \yii\db\ActiveRecord
{

    /**
     * Списки для генератора фейковых названий
     */
    private $name_prefix = ['ООО', 'ОАО', 'ЗАО', 'ИП', 'гос', 'издательство', 'комиссия', 'корпорация',];
    private $name1 = ['авто', 'мото', 'кино', 'фото', 'видео', 'физ', 'хим', 'сталь', 'металл', 'орто', 'пара', 'мета', 'био', 'квази'];
    private $name_postfix = ['', 'союз',];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'organization';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }


    /**
     * создаёт новую организацию генерируя название из трёх раннее определённых массивов
     * @return int id нового объекта
     */
    public function generateOrganization()
    {
        $name = "";
        $name .= $this->name_prefix[rand(0, count($this->name_prefix) - 1)];
        $name .= " " . implode('', array_slice($this->name1, rand(0, count($this->name1) - 1), rand(1, 3)));
        $name .= " " . $this->name_postfix[rand(0, count($this->name_postfix) - 1)];
        $this->name = $name;
        $this->save();
        return $this->id;
    }

    public function fields()
    {
        return [
            'id',
            'name',
            'phones',
            'categories'
        ];
    }

    public function getPhones()
    {
        return $this->hasMany(Phone::className(), ['id' => 'phone_id'])
            ->viaTable('organization_phone_xref', ['organization_id' => 'id']);
    }

    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])
            ->viaTable('organization_category_xref', ['organization_id' => 'id']);
    }

    public function getBuilding()
    {
        return $this->hasOne(Building::className(), ['id' => 'building_id'])
            ->viaTable('building_organization_xref', ['organization_id' => 'id']);
    }

    /**
     *
     * @return array|ActiveDataProvider
     * @throws Exception
     * @throws \yii\base\InvalidConfigException
     */
    public function getAllInRadius()
    {
        $requestParams = Yii::$app->getRequest()->getBodyParams();
        if (empty($requestParams)) {
            $requestParams = Yii::$app->getRequest()->getQueryParams();
        }

        if (!is_numeric($requestParams['radius']) || $requestParams['radius'] <= 0) {
            return ["error" => "Wrong radius parameter"];
        }
        // Здесь, по-хорошему, должна быть проверка на соответствие координат заданному формату
        // Не сделеал из-за недостатка времени, если потребуется сделаю

        $buildings = Building::find()
            ->select(['id'])
            ->inCircle($requestParams['center'], $requestParams['radius'])
            ->cache(6300)
            ->asArray()
            ->column();

        return $this->getOrganizationList($buildings);

    }

    public function getAllInRectalngle()
    {
        $requestParams = Yii::$app->getRequest()->getBodyParams();
        if (empty($requestParams)) {
            $requestParams = Yii::$app->getRequest()->getQueryParams();
        }

        $buildings_id = Building::find()
            ->select(['id'])
            ->inEnvelope($requestParams['coords'])
            ->cache(3600)
            ->asArray()
            ->column();
        return $this->getOrganizationList($buildings_id);
    }


    public function getAll()
    {
        return new ActiveDataProvider([
            'query' => self::find()->cache(3600),
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

    }

    public function getByCategory()
    {

        $requestParams = Yii::$app->getRequest()->getBodyParams();
        if (empty($requestParams)) {
            $requestParams = Yii::$app->getRequest()->getQueryParams();
        }
        if (!isset($requestParams['category'])) {
            return ['error' => 'No category param'];
        }
        $cat = $requestParams['category'];
        $categories = (new Category())->getTreePart($cat);
        $organizations = Organization::find()->joinWith('categories')->where(['IN', 'category_id', $categories])->cache(3600);
        return new ActiveDataProvider([
            'query' => $organizations,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
    }

    private function getOrganizationList($ids = [])
    {
        $organizations = self::find()->joinWith('building')->where(['IN', 'building_id', $ids]);
        return new ActiveDataProvider([
            'query' => $organizations,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

    }
}
