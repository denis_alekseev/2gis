<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "organization_phone_xref".
 *
 * @property int $id
 * @property int|null $organiation_id
 * @property int|null $phone_id
 */
class OrganizationPhoneXref extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'organization_phone_xref';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['organization_id', 'phone_id'], 'default', 'value' => null],
            [['organization_id', 'phone_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'organization_id' => 'Organization ID',
            'phone_id' => 'Phone ID',
        ];
    }
}
