<?php

namespace app\models;

use phpDocumentor\Reflection\Types\Integer;
use Yii;

/**
 * This is the model class for table "phone".
 *
 * @property int $id
 * @property string|null $phone
 */
class Phone extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'phone';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Phone',
        ];
    }

    public function generateNumber()
    {
        $this->phone = rand(100,999) . rand(100,999) . rand(1000,9999);
        $this->save();
        return $this->id;
    }

    public function fields() {
        return [
            'phone'
        ];
    }
}
