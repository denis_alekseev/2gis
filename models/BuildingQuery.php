<?php


namespace app\models;

use yii\db\ActiveQuery;

class BuildingQuery extends ActiveQuery
{
    public function inCircle($center, $radius)
    {
        return $this->where("st_distance(building.coord, st_point({$center}))<{$radius}");
    }

    public function inEnvelope($coords)
    {
        return $this->where("ST_Intersects(st_makeenvelope({$coords}), building.coord)");
    }
}