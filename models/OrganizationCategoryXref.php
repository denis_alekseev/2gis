<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "organization_category_xref".
 *
 * @property int $id
 * @property int|null $organization_id
 * @property int|null $category_id
 */
class OrganizationCategoryXref extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'organization_category_xref';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['organization_id', 'category_id'], 'default', 'value' => null],
            [['organization_id', 'category_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'organization_id' => 'Organization ID',
            'category_id' => 'Category ID',
        ];
    }
}
