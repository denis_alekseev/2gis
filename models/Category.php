<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $parent
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['path'], 'default', 'value' => null],
            [['parent'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'path' => 'Path',
        ];
    }

    public function fields()
    {
        return [
            'name'
        ];
    }

    public function getTreePart($id)
    {
        $curr = Category::findOne($id);
        if($curr->path == '') {

            $cond = "path <@ '{$curr->id}'";
        } else {
            $cond = "path <@ '" .$curr->path. "." .$curr->id . "'";
        }
        $query = "SELECT id FROM category WHERE {$cond}";

        $cats = array_map(function($data){return $data->id;},Category::findBySql($query)->all());
        $cats[] =  $curr->id;
        return $cats;
    }

    public function getOrganizations()
    {
        return $this->hasMany(Organization::className(),['id'=>'organization_id'])
            ->viaTable('organization_category_xref',['category_id'=>'id']);
    }
}
