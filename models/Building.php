<?php

namespace app\models;

use Yii;
use nanson\postgis\behaviors\GeometryBehavior;

/**
 * This is the model class for table "building".
 *
 * @property int $id
 * @property string|null $address
 */
class Building extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => GeometryBehavior::className(),
                'type' => GeometryBehavior::GEOMETRY_POINT,
                'attribute' => 'coord'
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'building';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address'], 'string', 'max' => 255],
            [['orgs'], 'array'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address' => 'Address',
            'orgs' => 'Organizations'
        ];
    }
    public static function find()
    {
        return new \app\models\BuildingQuery(get_called_class());
    }
    public function setOrgs($orgs)
    {
        $this->orgs = $orgs;
    }

    public function getOrgs()
    {
        return $this->orgs;
    }

    public function fields()
    {
        $res = [
            'id',
            'address',
            'coord' => function () {
                return $this->coord[0] . ',' . $this->coord[1];
            },
        ];
        if (!empty($this->orgs)) {
            $res[]= 'orgs';
        }
        return $res;
    }

    public function getOrganizations()
    {
        return $this->hasMany(Organization::className(), ['id' => 'organization_id'])
            ->viaTable('building_organization_xref', ['building_id' => 'id']);
    }

}
