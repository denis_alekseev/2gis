<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "building_organization_xref".
 *
 * @property int $id
 * @property int|null $organization_id
 * @property int|null $building_id
 */
class BuildingOrganizationXref extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'building_organization_xref';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['organization_id', 'building_id'], 'default', 'value' => null],
            [['organization_id', 'building_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'organization_id' => 'Organization ID',
            'building_id' => 'Building ID',
        ];
    }
}
