<?php

use yii\db\Migration;

/**
 * Class m191205_161832_create_building_data
 */
class m191205_161832_create_building_data extends Migration
{

    private $num = 0;
    private const MAX = 1000; // ���������� �������� ������� �� ������� �� ������� ������ ������� �� �����, � �� �������
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        while($this->num < self::MAX)
        {
            $this->getCoords();
        }
    }

    private function getCoords() 
    {
        //�������� � ������� ����� ��������� ����� �����
        $lat = rand(731044,736434)/10000;
        $long = rand(548537,550310)/10000;
        $url = "https://geocode-maps.yandex.ru/1.x/?apikey=eb656d43-92a0-44aa-9152-b3c02325dc57&geocode={$lat},{$long}&format=json&kind=house&results=100";

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => [],
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
            $data = json_decode($response);
            $found = $data->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found;
            if($found>0) {
                    foreach($data->response->GeoObjectCollection->featureMember as $item) {
                    print_r([$item->GeoObject->name, $item->GeoObject->Point->pos]);
                    $this->insert('building',[
                        'address' => $item->GeoObject->name,
                        'coord' => 'POINT(' . $item->GeoObject->Point->pos . ')',
                    ]);
                    $this->num++;

                }
            }
        }        

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute("TRUNCATE TABLE building");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191205_161832_create_building_data cannot be reverted.\n";

        return false;
    }
    */
}
