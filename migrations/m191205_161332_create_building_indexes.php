<?php

use yii\db\Migration;

/**
 * Class m191205_161332_create_building_indexes
 */
class m191205_161332_create_building_indexes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE INDEX coord_gist_building_idx ON building USING GIST(coord)");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute("DROP INDEX coord_gist_building_idx");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191205_161332_create_building_indexes cannot be reverted.\n";

        return false;
    }
    */
}
