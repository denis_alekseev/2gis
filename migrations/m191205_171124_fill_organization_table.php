<?php

use yii\db\Migration;
use app\models\Phone;
use app\models\Organization;

/**
 * Class m191205_171124_fill_organization_table
 */
class m191205_171124_fill_organization_table extends Migration
{
    private $currentId;
    /**
     * {@inheritdoc}
     */

    public function safeUp()
    {
        for($i=0;$i<100000;$i++) {
            $org = new Organization();
            $this->currentId = $org->generateOrganization();
            $this->assignToCategory();
            $this->assignToBuilding();
            $this->assignToPhone();
            unset($org);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        //echo "m191205_171124_fill_organization_table cannot be reverted.\n";
        $this->truncateTable('organization');
        $this->truncateTable('organization_category_xref');
        $this->truncateTable('building_organization_xref');
        $this->truncateTable('phone');
        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191205_171124_fill_organization_table cannot be reverted.\n";

        return false;
    }
    */
    private function assignToCategory()
    {
        $cats = (new yii\db\Query())
            ->select(['id'])
            ->from('category')
            ->limit(rand(1,5))
            ->orderBy('random()')
            ->all();
        $xref = [];
        foreach( $cats as $item) {
            $xref[]=[$this->currentId,$item['id']];
        }
        $this->batchInsert('organization_category_xref',['organization_id','category_id'],$xref);
    }

    private function assignToBuilding()
    {
        $building = (new yii\db\Query())
            ->select(['id'])
            ->from('building')
            ->limit(1)
            ->orderBy('random()')
            ->all();
        $this->insert('building_organization_xref',[
            'organization_id' => $this->currentId,
            'building_id' => $building[0]['id']
        ]);
    }

    private function assignToPhone()
    {
        $arr = [];
        $num = rand(1,3);
        for($i=0;$i<=$num;$i++) {
            $phone = new Phone();
            $phone->generateNumber();
            $arr[]=[$this->currentId, $phone->id];
            unset($phone);
        }
        $this->batchInsert('organization_phone_xref',['organization_id','phone_id'],$arr);
    }
}
