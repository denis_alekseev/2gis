<?php

use yii\db\Migration;

/**
 * Class m191203_155334_create_category_data
 */
class m191203_155334_create_category_data extends Migration
{
    private $current = 0;
    private const MAX_CATEGORY = 1000;
    /**
     * {@inheritdoc}
     * �������� ���� � ��� ��� �� ������� ����� 1000 ���������. ������ ��������� � ��������� ������������ ����� ������� �� �������� ����������
     * �� ����� ����� ������������ ����� �������� ������������� �����������
     */
    public function safeUp()
    {
        while($this->current <= self::MAX_CATEGORY) 
        {
            $this->makeCategory();
        }
    }


    private function makeCategory($path=[]) 
    {
        $this->insert('category',[
            'name' => 'Category ' . $this->current,
            'path' => implode('.',$path)
        ]);
        $this->current++;
        $val = rand(0,100);
        if($val <= 50 && $this->current <= self::MAX_CATEGORY) 
        {
            $path[]=$this->current;
            $this->makeCategory($path);
        }
    }
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute("TRUNCATE TABLE category");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191203_155334_create_category_data cannot be reverted.\n";

        return false;
    }
    */
}
