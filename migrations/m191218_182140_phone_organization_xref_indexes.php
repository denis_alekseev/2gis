<?php

use yii\db\Migration;

/**
 * Class m191218_182140_phone_organization_xref_indexes
 */
class m191218_182140_phone_organization_xref_indexes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('organizations_phone', 'organization_phone_xref','organization_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('organizations_phone', 'organization_phone_xref');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191218_182140_phone_organization_xref_indexes cannot be reverted.\n";

        return false;
    }
    */
}
