<?php

use yii\db\Migration;

/**
 * Class m191203_152118_create_category_indexes
 */
class m191203_152118_create_category_indexes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE INDEX path_gist_category_idx ON category USING GIST(path)");
        $this->execute("CREATE INDEX path_category_idx ON category USING btree(path)");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute("DROP INDEX path_gist_category_idx");
        $this->execute("DROP INDEX path_category_idx");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191203_152118_create_category_indexes cannot be reverted.\n";

        return false;
    }
    */
}
