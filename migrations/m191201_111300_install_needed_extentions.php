<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%organizations}}`.
 */
class m191201_111300_install_needed_extentions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE EXTENSION ltree");
        $this->execute("CREATE EXTENSION postgis;");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute("DROP EXTENSION ltree");
        $this->execute("DROP EXTENSION postgis;");
    }
}
