<?php

use yii\db\Migration;

/**
 * Class m191218_181726_organization_xref_indexes
 */
class m191218_181726_organization_xref_indexes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('organizations', 'building_organization_xref','organization_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('organizations', 'building_organization_xref');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191218_181726_organization_xref_indexes cannot be reverted.\n";

        return false;
    }
    */
}
