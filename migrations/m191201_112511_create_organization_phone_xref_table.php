<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%organization_phone_xref}}`.
 */
class m191201_112511_create_organization_phone_xref_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('organization_phone_xref', [
            'id' => $this->primaryKey(),
            'organization_id' => $this->integer(),
            'phone_id' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('organization_phone_xref');
    }
}
