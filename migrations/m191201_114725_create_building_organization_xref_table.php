<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%building_organization_xref}}`.
 */
class m191201_114725_create_building_organization_xref_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('building_organization_xref', [
            'id' => $this->primaryKey(),
            'organization_id' => $this->integer(),
            'building_id' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('building_organization_xref');
    }
}
