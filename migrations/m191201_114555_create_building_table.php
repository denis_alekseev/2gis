<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%building}}`.
 */
class m191201_114555_create_building_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('building', [
            'id' => $this->primaryKey(),
            'address' => $this->string(),
            'coord' => 'geography(POINT,4326)',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('building');
    }
}
