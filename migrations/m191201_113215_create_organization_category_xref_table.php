<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%organization_category_xref}}`.
 */
class m191201_113215_create_organization_category_xref_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('organization_category_xref', [
            'id' => $this->primaryKey(),
            'organization_id' => $this->integer(),
            'category_id' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%organization_category_xref}}');
    }
}
